import React from 'react';
import Navigator from '../navigator';
import MainImage from '../main-image';
import Content from '../content';
import Footer from '../footer';

import './main.css';

const Main = ({navList}) => {
    return (
        <div className='main_div'>
            <Navigator navList={navList}></Navigator>
            <MainImage></MainImage>
            <Content></Content>
            <Footer></Footer>
        </div>
    )
}

export default Main;
import React from 'react';

import './footer.css';

const Footer = () => {
    return(
        <footer>
            <div className='footer_div'>Copyright by phototime - all right reserved</div>
        </footer>
    )
}

export default Footer;
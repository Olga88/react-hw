import React from 'react';

import './navigator.css';

const Navigator = ({navList}) => {
    return (
        <div>
            <ul className='navigator'>
                {navList.map((nav, index)=>{
                    return(
                        <li key={index * 5 + 'b'}>{nav}</li>
                    )
                })}
            </ul>
        </div>
    )
}

export default Navigator;
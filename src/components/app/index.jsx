import React from 'react';
import Main from '../main';

import './app.css';

const App = () => {
    const navList = ['home', 'photoapp', 'design', 'download'];
    return(
        <div className='container'>
            <Main navList={navList}></Main>
        </div>
    )
}

export default App;
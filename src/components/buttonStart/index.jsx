import React from 'react';

import './buttonStart.css';

const ButtonStart = () => {
    return(
        <div className='button_div'>
            <input className='but_css' type="button" value="get started"/>
        </div>
    )
}

export default ButtonStart;
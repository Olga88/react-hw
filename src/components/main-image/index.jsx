import React from 'react';
import Img from '../img/top-img@1X.png'

const MainImage = () => {
    return(
        <div>
            <img src={Img} alt='phone'/>
        </div>
    )
}
export default MainImage;